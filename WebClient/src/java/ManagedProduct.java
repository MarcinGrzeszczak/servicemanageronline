/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import dto.CostDto;
import dto.HistoryDto;
import ejb.WebClientEjbRemote;
import java.awt.event.ActionEvent;

import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionListener;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Wsiz
 */
@Named(value = "managedProduct")
@SessionScoped
public class ManagedProduct implements Serializable{

    @EJB
    private WebClientEjbRemote webClientEjb;
    private boolean isWrongCredentials;
    
    private String login = "";
    private String token = "";
    
    private int status;
    private String serviceName;
    private String devName;
    private String serialNumber;
    private String ordNmb;
    private Date dueDate;
    private DataModel details;
    private String sum;
    private boolean isEmptyFields;
    private List<List<String>> data;
    private HistoryDto hist;
    
    public ManagedProduct() {
        this.isWrongCredentials = false;
        this.isEmptyFields = false;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
    
    public DataModel getDetails() {
        return details;
    }

    public void setDetails(DataModel details) {
        this.details = details;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }
    
    
    
    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    
    
    
    public boolean isIsEmptyFields() {
        return isEmptyFields;
    }

    public void setIsEmptyFields(boolean isEmptyFields) {
        this.isEmptyFields = isEmptyFields;
    }

    public WebClientEjbRemote getWebClientEjb() {
        return webClientEjb;
    }

    public void setWebClientEjb(WebClientEjbRemote webClientEjb) {
        this.webClientEjb = webClientEjb;
    }

    public String getDevName() {
        return devName;
    }

    public void setDevName(String devName) {
        this.devName = devName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getOrdNmb() {
        return ordNmb;
    }

    public void setOrdNmb(String ordNmb) {
        this.ordNmb = ordNmb;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
    
    
    
    public boolean isIsWrongCredentials() {
        return isWrongCredentials;
    }

    public void setIsWrongCredentials(boolean isWrongCredentials) {
        this.isWrongCredentials = isWrongCredentials;
    }
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
     
    public String validateUser() {
        System.out.println("CCCCCCCCCCCCCCCCCCCCCCC");
        if(login.isEmpty() || token.isEmpty()){
            this.isEmptyFields = true;
            return "index";
        }
        this.hist = webClientEjb.loginClient(login, token);
        if( hist != null){
            this.serviceName = hist.getName();
            this.ordNmb = hist.getOrderNumber();
            this.dueDate = hist.getDueDate();
            this.devName = hist.getDevice().getName();
            this.serialNumber = hist.getDevice().getSerialNumber();
            this.status = hist.getStatus();
            List<CostDto> costs = webClientEjb.getHistoryCosts(hist.getId());
            this.calculateSum(costs);
            this.generateDetails(costs);
            return "main";
        }
        this.isWrongCredentials = true;
        return "index";
    }
    
    private void calculateSum(List<CostDto> costs) {

        double tmp = costs.stream().
                map(cost -> (double) cost.getPrice() * (100 - cost.getDiscount())/100).
                reduce(0d, (e1, e2) -> e1+e2);
        this.sum = String.valueOf(tmp);
    }
    
    public void generateDetails(List<CostDto> costs) {
        this.data = costs.stream()
                .map(item -> this.costs2List(item))
                .collect(Collectors.toList());
       
        this.details = new ListDataModel(data);
    }
    
    private static List<String> costs2List(CostDto cost) {
        List<String> list = new ArrayList();
        list.add(String.valueOf(cost.getId()));
        list.add(cost.getName());
        list.add(String.valueOf(cost.getPrice()));
        list.add(String.valueOf(cost.getVat()));
        list.add(String.valueOf(cost.getDiscount()));
        list.add(cost.getDescription());
        list.add(String.valueOf(cost.isIsAccepted()));

        return list;
    }
    
    public String updateAcceptStatus(){
        System.out.println("BBBBBBBB");
        FacesContext facesContext = FacesContext. getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        Map params = externalContext.getRequestParameterMap();
        Integer id = new Integer((String) params.get("id" ));
        boolean value = params.get("value").equals("true");
        System.out.println("ID: "+id+" VALUE: "+value);
        webClientEjb.updateCostAcceptaction(id, value);
        
        List<CostDto> costs = webClientEjb.getHistoryCosts(hist.getId());
        this.generateDetails(costs);
        return "";
    }
}
