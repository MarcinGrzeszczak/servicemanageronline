/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Devices;
import entities.Tokens;
import entities.Costs;
import entities.History;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Wsiz
 */
public class HistoryJpaController implements Serializable {

    public HistoryJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(History history) {
        if (history.getCostsList() == null) {
            history.setCostsList(new ArrayList<Costs>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Devices parentId = history.getParentId();
            if (parentId != null) {
                parentId = em.getReference(parentId.getClass(), parentId.getId());
                history.setParentId(parentId);
            }
            Tokens tokens = history.getTokens();
            if (tokens != null) {
                tokens = em.getReference(tokens.getClass(), tokens.getId());
                history.setTokens(tokens);
            }
            List<Costs> attachedCostsList = new ArrayList<Costs>();
            for (Costs costsListCostsToAttach : history.getCostsList()) {
                costsListCostsToAttach = em.getReference(costsListCostsToAttach.getClass(), costsListCostsToAttach.getId());
                attachedCostsList.add(costsListCostsToAttach);
            }
            history.setCostsList(attachedCostsList);
            em.persist(history);
            if (parentId != null) {
                parentId.getHistoryList().add(history);
                parentId = em.merge(parentId);
            }
            if (tokens != null) {
                History oldHistoryOfTokens = tokens.getHistory();
                if (oldHistoryOfTokens != null) {
                    oldHistoryOfTokens.setTokens(null);
                    oldHistoryOfTokens = em.merge(oldHistoryOfTokens);
                }
                tokens.setHistory(history);
                tokens = em.merge(tokens);
            }
            for (Costs costsListCosts : history.getCostsList()) {
                History oldParentIdOfCostsListCosts = costsListCosts.getParentId();
                costsListCosts.setParentId(history);
                costsListCosts = em.merge(costsListCosts);
                if (oldParentIdOfCostsListCosts != null) {
                    oldParentIdOfCostsListCosts.getCostsList().remove(costsListCosts);
                    oldParentIdOfCostsListCosts = em.merge(oldParentIdOfCostsListCosts);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(History history) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            History persistentHistory = em.find(History.class, history.getId());
            Devices parentIdOld = persistentHistory.getParentId();
            Devices parentIdNew = history.getParentId();
            Tokens tokensOld = persistentHistory.getTokens();
            Tokens tokensNew = history.getTokens();
            List<Costs> costsListOld = persistentHistory.getCostsList();
            List<Costs> costsListNew = history.getCostsList();
            List<String> illegalOrphanMessages = null;
            if (tokensOld != null && !tokensOld.equals(tokensNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain Tokens " + tokensOld + " since its history field is not nullable.");
            }
            for (Costs costsListOldCosts : costsListOld) {
                if (!costsListNew.contains(costsListOldCosts)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Costs " + costsListOldCosts + " since its parentId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (parentIdNew != null) {
                parentIdNew = em.getReference(parentIdNew.getClass(), parentIdNew.getId());
                history.setParentId(parentIdNew);
            }
            if (tokensNew != null) {
                tokensNew = em.getReference(tokensNew.getClass(), tokensNew.getId());
                history.setTokens(tokensNew);
            }
            List<Costs> attachedCostsListNew = new ArrayList<Costs>();
            for (Costs costsListNewCostsToAttach : costsListNew) {
                costsListNewCostsToAttach = em.getReference(costsListNewCostsToAttach.getClass(), costsListNewCostsToAttach.getId());
                attachedCostsListNew.add(costsListNewCostsToAttach);
            }
            costsListNew = attachedCostsListNew;
            history.setCostsList(costsListNew);
            history = em.merge(history);
            if (parentIdOld != null && !parentIdOld.equals(parentIdNew)) {
                parentIdOld.getHistoryList().remove(history);
                parentIdOld = em.merge(parentIdOld);
            }
            if (parentIdNew != null && !parentIdNew.equals(parentIdOld)) {
                parentIdNew.getHistoryList().add(history);
                parentIdNew = em.merge(parentIdNew);
            }
            if (tokensNew != null && !tokensNew.equals(tokensOld)) {
                History oldHistoryOfTokens = tokensNew.getHistory();
                if (oldHistoryOfTokens != null) {
                    oldHistoryOfTokens.setTokens(null);
                    oldHistoryOfTokens = em.merge(oldHistoryOfTokens);
                }
                tokensNew.setHistory(history);
                tokensNew = em.merge(tokensNew);
            }
            for (Costs costsListNewCosts : costsListNew) {
                if (!costsListOld.contains(costsListNewCosts)) {
                    History oldParentIdOfCostsListNewCosts = costsListNewCosts.getParentId();
                    costsListNewCosts.setParentId(history);
                    costsListNewCosts = em.merge(costsListNewCosts);
                    if (oldParentIdOfCostsListNewCosts != null && !oldParentIdOfCostsListNewCosts.equals(history)) {
                        oldParentIdOfCostsListNewCosts.getCostsList().remove(costsListNewCosts);
                        oldParentIdOfCostsListNewCosts = em.merge(oldParentIdOfCostsListNewCosts);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = history.getId();
                if (findHistory(id) == null) {
                    throw new NonexistentEntityException("The history with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            History history;
            try {
                history = em.getReference(History.class, id);
                history.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The history with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Tokens tokensOrphanCheck = history.getTokens();
            if (tokensOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This History (" + history + ") cannot be destroyed since the Tokens " + tokensOrphanCheck + " in its tokens field has a non-nullable history field.");
            }
            List<Costs> costsListOrphanCheck = history.getCostsList();
            for (Costs costsListOrphanCheckCosts : costsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This History (" + history + ") cannot be destroyed since the Costs " + costsListOrphanCheckCosts + " in its costsList field has a non-nullable parentId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Devices parentId = history.getParentId();
            if (parentId != null) {
                parentId.getHistoryList().remove(history);
                parentId = em.merge(parentId);
            }
            em.remove(history);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<History> findHistoryEntities() {
        return findHistoryEntities(true, -1, -1);
    }

    public List<History> findHistoryEntities(int maxResults, int firstResult) {
        return findHistoryEntities(false, maxResults, firstResult);
    }

    private List<History> findHistoryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(History.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public History findHistory(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(History.class, id);
        } finally {
            em.close();
        }
    }

    public int getHistoryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<History> rt = cq.from(History.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
