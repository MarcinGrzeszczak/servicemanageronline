/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import entities.Costs;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.History;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Wsiz
 */
public class CostsJpaController implements Serializable {

    public CostsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Costs costs) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            History parentId = costs.getParentId();
            if (parentId != null) {
                parentId = em.getReference(parentId.getClass(), parentId.getId());
                costs.setParentId(parentId);
            }
            em.persist(costs);
            if (parentId != null) {
                parentId.getCostsList().add(costs);
                parentId = em.merge(parentId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Costs costs) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Costs persistentCosts = em.find(Costs.class, costs.getId());
            History parentIdOld = persistentCosts.getParentId();
            History parentIdNew = costs.getParentId();
            if (parentIdNew != null) {
                parentIdNew = em.getReference(parentIdNew.getClass(), parentIdNew.getId());
                costs.setParentId(parentIdNew);
            }
            costs = em.merge(costs);
            if (parentIdOld != null && !parentIdOld.equals(parentIdNew)) {
                parentIdOld.getCostsList().remove(costs);
                parentIdOld = em.merge(parentIdOld);
            }
            if (parentIdNew != null && !parentIdNew.equals(parentIdOld)) {
                parentIdNew.getCostsList().add(costs);
                parentIdNew = em.merge(parentIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = costs.getId();
                if (findCosts(id) == null) {
                    throw new NonexistentEntityException("The costs with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Costs costs;
            try {
                costs = em.getReference(Costs.class, id);
                costs.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The costs with id " + id + " no longer exists.", enfe);
            }
            History parentId = costs.getParentId();
            if (parentId != null) {
                parentId.getCostsList().remove(costs);
                parentId = em.merge(parentId);
            }
            em.remove(costs);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Costs> findCostsEntities() {
        return findCostsEntities(true, -1, -1);
    }

    public List<Costs> findCostsEntities(int maxResults, int firstResult) {
        return findCostsEntities(false, maxResults, firstResult);
    }

    private List<Costs> findCostsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Costs.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Costs findCosts(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Costs.class, id);
        } finally {
            em.close();
        }
    }

    public int getCostsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Costs> rt = cq.from(Costs.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
