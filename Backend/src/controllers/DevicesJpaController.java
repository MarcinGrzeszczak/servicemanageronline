/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Clients;
import entities.Devices;
import entities.History;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Wsiz
 */
public class DevicesJpaController implements Serializable {

    public DevicesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Devices devices) {
        if (devices.getHistoryList() == null) {
            devices.setHistoryList(new ArrayList<History>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Clients parentId = devices.getParentId();
            if (parentId != null) {
                parentId = em.getReference(parentId.getClass(), parentId.getId());
                devices.setParentId(parentId);
            }
            List<History> attachedHistoryList = new ArrayList<History>();
            for (History historyListHistoryToAttach : devices.getHistoryList()) {
                historyListHistoryToAttach = em.getReference(historyListHistoryToAttach.getClass(), historyListHistoryToAttach.getId());
                attachedHistoryList.add(historyListHistoryToAttach);
            }
            devices.setHistoryList(attachedHistoryList);
            em.persist(devices);
            if (parentId != null) {
                parentId.getDevicesList().add(devices);
                parentId = em.merge(parentId);
            }
            for (History historyListHistory : devices.getHistoryList()) {
                Devices oldParentIdOfHistoryListHistory = historyListHistory.getParentId();
                historyListHistory.setParentId(devices);
                historyListHistory = em.merge(historyListHistory);
                if (oldParentIdOfHistoryListHistory != null) {
                    oldParentIdOfHistoryListHistory.getHistoryList().remove(historyListHistory);
                    oldParentIdOfHistoryListHistory = em.merge(oldParentIdOfHistoryListHistory);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Devices devices) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Devices persistentDevices = em.find(Devices.class, devices.getId());
            Clients parentIdOld = persistentDevices.getParentId();
            Clients parentIdNew = devices.getParentId();
            List<History> historyListOld = persistentDevices.getHistoryList();
            List<History> historyListNew = devices.getHistoryList();
            List<String> illegalOrphanMessages = null;
            for (History historyListOldHistory : historyListOld) {
                if (!historyListNew.contains(historyListOldHistory)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain History " + historyListOldHistory + " since its parentId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (parentIdNew != null) {
                parentIdNew = em.getReference(parentIdNew.getClass(), parentIdNew.getId());
                devices.setParentId(parentIdNew);
            }
            List<History> attachedHistoryListNew = new ArrayList<History>();
            for (History historyListNewHistoryToAttach : historyListNew) {
                historyListNewHistoryToAttach = em.getReference(historyListNewHistoryToAttach.getClass(), historyListNewHistoryToAttach.getId());
                attachedHistoryListNew.add(historyListNewHistoryToAttach);
            }
            historyListNew = attachedHistoryListNew;
            devices.setHistoryList(historyListNew);
            devices = em.merge(devices);
            if (parentIdOld != null && !parentIdOld.equals(parentIdNew)) {
                parentIdOld.getDevicesList().remove(devices);
                parentIdOld = em.merge(parentIdOld);
            }
            if (parentIdNew != null && !parentIdNew.equals(parentIdOld)) {
                parentIdNew.getDevicesList().add(devices);
                parentIdNew = em.merge(parentIdNew);
            }
            for (History historyListNewHistory : historyListNew) {
                if (!historyListOld.contains(historyListNewHistory)) {
                    Devices oldParentIdOfHistoryListNewHistory = historyListNewHistory.getParentId();
                    historyListNewHistory.setParentId(devices);
                    historyListNewHistory = em.merge(historyListNewHistory);
                    if (oldParentIdOfHistoryListNewHistory != null && !oldParentIdOfHistoryListNewHistory.equals(devices)) {
                        oldParentIdOfHistoryListNewHistory.getHistoryList().remove(historyListNewHistory);
                        oldParentIdOfHistoryListNewHistory = em.merge(oldParentIdOfHistoryListNewHistory);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = devices.getId();
                if (findDevices(id) == null) {
                    throw new NonexistentEntityException("The devices with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Devices devices;
            try {
                devices = em.getReference(Devices.class, id);
                devices.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The devices with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<History> historyListOrphanCheck = devices.getHistoryList();
            for (History historyListOrphanCheckHistory : historyListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Devices (" + devices + ") cannot be destroyed since the History " + historyListOrphanCheckHistory + " in its historyList field has a non-nullable parentId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Clients parentId = devices.getParentId();
            if (parentId != null) {
                parentId.getDevicesList().remove(devices);
                parentId = em.merge(parentId);
            }
            em.remove(devices);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Devices> findDevicesEntities() {
        return findDevicesEntities(true, -1, -1);
    }

    public List<Devices> findDevicesEntities(int maxResults, int firstResult) {
        return findDevicesEntities(false, maxResults, firstResult);
    }

    private List<Devices> findDevicesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Devices.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Devices findDevices(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Devices.class, id);
        } finally {
            em.close();
        }
    }

    public int getDevicesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Devices> rt = cq.from(Devices.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
