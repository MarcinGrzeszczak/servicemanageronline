/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.History;
import entities.Tokens;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Wsiz
 */
public class TokensJpaController implements Serializable {

    public TokensJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Tokens tokens) throws IllegalOrphanException, PreexistingEntityException, Exception {
        List<String> illegalOrphanMessages = null;
        History historyOrphanCheck = tokens.getHistory();
        if (historyOrphanCheck != null) {
            Tokens oldTokensOfHistory = historyOrphanCheck.getTokens();
            if (oldTokensOfHistory != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("The History " + historyOrphanCheck + " already has an item of type Tokens whose history column cannot be null. Please make another selection for the history field.");
            }
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            History history = tokens.getHistory();
            if (history != null) {
                history = em.getReference(history.getClass(), history.getId());
                tokens.setHistory(history);
            }
            em.persist(tokens);
            if (history != null) {
                history.setTokens(tokens);
                history = em.merge(history);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTokens(tokens.getId()) != null) {
                throw new PreexistingEntityException("Tokens " + tokens + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Tokens tokens) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tokens persistentTokens = em.find(Tokens.class, tokens.getId());
            History historyOld = persistentTokens.getHistory();
            History historyNew = tokens.getHistory();
            List<String> illegalOrphanMessages = null;
            if (historyNew != null && !historyNew.equals(historyOld)) {
                Tokens oldTokensOfHistory = historyNew.getTokens();
                if (oldTokensOfHistory != null) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("The History " + historyNew + " already has an item of type Tokens whose history column cannot be null. Please make another selection for the history field.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (historyNew != null) {
                historyNew = em.getReference(historyNew.getClass(), historyNew.getId());
                tokens.setHistory(historyNew);
            }
            tokens = em.merge(tokens);
            if (historyOld != null && !historyOld.equals(historyNew)) {
                historyOld.setTokens(null);
                historyOld = em.merge(historyOld);
            }
            if (historyNew != null && !historyNew.equals(historyOld)) {
                historyNew.setTokens(tokens);
                historyNew = em.merge(historyNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tokens.getId();
                if (findTokens(id) == null) {
                    throw new NonexistentEntityException("The tokens with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tokens tokens;
            try {
                tokens = em.getReference(Tokens.class, id);
                tokens.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tokens with id " + id + " no longer exists.", enfe);
            }
            History history = tokens.getHistory();
            if (history != null) {
                history.setTokens(null);
                history = em.merge(history);
            }
            em.remove(tokens);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Tokens> findTokensEntities() {
        return findTokensEntities(true, -1, -1);
    }

    public List<Tokens> findTokensEntities(int maxResults, int firstResult) {
        return findTokensEntities(false, maxResults, firstResult);
    }

    private List<Tokens> findTokensEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Tokens.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Tokens findTokens(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Tokens.class, id);
        } finally {
            em.close();
        }
    }

    public int getTokensCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Tokens> rt = cq.from(Tokens.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
