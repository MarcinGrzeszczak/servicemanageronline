/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Clients;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Devices;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Wsiz
 */
public class ClientsJpaController implements Serializable {

    public ClientsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Clients clients) {
        if (clients.getDevicesList() == null) {
            clients.setDevicesList(new ArrayList<Devices>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Devices> attachedDevicesList = new ArrayList<Devices>();
            for (Devices devicesListDevicesToAttach : clients.getDevicesList()) {
                devicesListDevicesToAttach = em.getReference(devicesListDevicesToAttach.getClass(), devicesListDevicesToAttach.getId());
                attachedDevicesList.add(devicesListDevicesToAttach);
            }
            clients.setDevicesList(attachedDevicesList);
            em.persist(clients);
            for (Devices devicesListDevices : clients.getDevicesList()) {
                Clients oldParentIdOfDevicesListDevices = devicesListDevices.getParentId();
                devicesListDevices.setParentId(clients);
                devicesListDevices = em.merge(devicesListDevices);
                if (oldParentIdOfDevicesListDevices != null) {
                    oldParentIdOfDevicesListDevices.getDevicesList().remove(devicesListDevices);
                    oldParentIdOfDevicesListDevices = em.merge(oldParentIdOfDevicesListDevices);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Clients clients) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Clients persistentClients = em.find(Clients.class, clients.getId());
            List<Devices> devicesListOld = persistentClients.getDevicesList();
            List<Devices> devicesListNew = clients.getDevicesList();
            List<String> illegalOrphanMessages = null;
            for (Devices devicesListOldDevices : devicesListOld) {
                if (!devicesListNew.contains(devicesListOldDevices)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Devices " + devicesListOldDevices + " since its parentId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Devices> attachedDevicesListNew = new ArrayList<Devices>();
            for (Devices devicesListNewDevicesToAttach : devicesListNew) {
                devicesListNewDevicesToAttach = em.getReference(devicesListNewDevicesToAttach.getClass(), devicesListNewDevicesToAttach.getId());
                attachedDevicesListNew.add(devicesListNewDevicesToAttach);
            }
            devicesListNew = attachedDevicesListNew;
            clients.setDevicesList(devicesListNew);
            clients = em.merge(clients);
            for (Devices devicesListNewDevices : devicesListNew) {
                if (!devicesListOld.contains(devicesListNewDevices)) {
                    Clients oldParentIdOfDevicesListNewDevices = devicesListNewDevices.getParentId();
                    devicesListNewDevices.setParentId(clients);
                    devicesListNewDevices = em.merge(devicesListNewDevices);
                    if (oldParentIdOfDevicesListNewDevices != null && !oldParentIdOfDevicesListNewDevices.equals(clients)) {
                        oldParentIdOfDevicesListNewDevices.getDevicesList().remove(devicesListNewDevices);
                        oldParentIdOfDevicesListNewDevices = em.merge(oldParentIdOfDevicesListNewDevices);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = clients.getId();
                if (findClients(id) == null) {
                    throw new NonexistentEntityException("The clients with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Clients clients;
            try {
                clients = em.getReference(Clients.class, id);
                clients.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The clients with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Devices> devicesListOrphanCheck = clients.getDevicesList();
            for (Devices devicesListOrphanCheckDevices : devicesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Clients (" + clients + ") cannot be destroyed since the Devices " + devicesListOrphanCheckDevices + " in its devicesList field has a non-nullable parentId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(clients);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Clients> findClientsEntities() {
        return findClientsEntities(true, -1, -1);
    }

    public List<Clients> findClientsEntities(int maxResults, int firstResult) {
        return findClientsEntities(false, maxResults, firstResult);
    }

    private List<Clients> findClientsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Clients.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Clients findClients(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Clients.class, id);
        } finally {
            em.close();
        }
    }

    public int getClientsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Clients> rt = cq.from(Clients.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
