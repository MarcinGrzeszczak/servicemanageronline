/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Wsiz
 */
@Entity
@Table(name = "costs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Costs.findAll", query = "SELECT c FROM Costs c"),
    @NamedQuery(name = "Costs.findById", query = "SELECT c FROM Costs c WHERE c.id = :id"),
    @NamedQuery(name = "Costs.findByName", query = "SELECT c FROM Costs c WHERE c.name = :name"),
    @NamedQuery(name = "Costs.findByPrice", query = "SELECT c FROM Costs c WHERE c.price = :price"),
    @NamedQuery(name = "Costs.findByVat", query = "SELECT c FROM Costs c WHERE c.vat = :vat"),
    @NamedQuery(name = "Costs.findByDiscount", query = "SELECT c FROM Costs c WHERE c.discount = :discount"),
    @NamedQuery(name = "Costs.findByDescription", query = "SELECT c FROM Costs c WHERE c.description = :description")})
public class Costs implements Serializable {

    @Column(name = "is_accepted")
    private Boolean isAccepted;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "price")
    private float price;
    @Basic(optional = false)
    @Column(name = "vat")
    private float vat;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "discount")
    private Float discount;
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private History parentId;

    public Costs() {
    }

    public Costs(Integer id) {
        this.id = id;
    }

    public Costs(Integer id, String name, float price, float vat) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.vat = vat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getVat() {
        return vat;
    }

    public void setVat(float vat) {
        this.vat = vat;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public History getParentId() {
        return parentId;
    }

    public void setParentId(History parentId) {
        this.parentId = parentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Costs)) {
            return false;
        }
        Costs other = (Costs) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Costs[ id=" + id + " ]";
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }
    
}
