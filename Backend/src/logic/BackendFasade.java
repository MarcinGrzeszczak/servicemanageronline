/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import controllers.ClientsJpaController;
import controllers.CostsJpaController;
import controllers.DevicesJpaController;
import controllers.HistoryJpaController;
import controllers.TokensJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import controllers.exceptions.PreexistingEntityException;
import dto.ClientDto;
import dto.CostDto;
import dto.DeviceDto;
import dto.HistoryDto;
import entities.Clients;
import entities.Costs;
import entities.Devices;
import entities.History;
import entities.Tokens;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import utils.Utils;

public class BackendFasade {
    private static final String PERSISTENCE_UNIT_NAME = "BackendPU";
    
    private final ClientsJpaController clientsController;
    private final CostsJpaController costsController;
    private final DevicesJpaController devicesController;
    private final HistoryJpaController historyController;
    private final TokensJpaController tokensController;
    
    private Clients selectedClient;
    private Devices selectedDevice;
    private History selectedHistory;
    
    public BackendFasade(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        clientsController = new ClientsJpaController(factory);
        costsController = new CostsJpaController(factory);
        devicesController = new DevicesJpaController(factory);
        historyController = new HistoryJpaController(factory);
        tokensController = new TokensJpaController(factory);
    }
    
    
    public List<ClientDto> getAllClients() {
        List<Clients> clients = clientsController.findClientsEntities();
        List<ClientDto> clientsDto = new ArrayList();
        clients.forEach((client) -> clientsDto.add(this.clientEntityToDto(client)));   
        
        return clientsDto;
    }
    
    public List<DeviceDto> getClientDevices() {
       List<Devices> devices = this.selectedClient.getDevicesList();
       List<DeviceDto> devicesDto = new ArrayList();
       
       devices.forEach((device) -> devicesDto.add(this.deviceEntityToDto(device)));
       
       return devicesDto;
    }
    
    public List<HistoryDto> getDeviceHistory() {
        List<History> history = this.selectedDevice.getHistoryList();
        
        List<HistoryDto> historyDto = new ArrayList();
        
        history.forEach(hist ->  historyDto.add(this.historyEntityToDto(hist)));
        
        return historyDto;
    }
    
    public List<CostDto> getHistoryCosts() {
        List<Costs> costs = this.selectedHistory.getCostsList();
        List<CostDto> costsDto = new ArrayList();
        
        costs.forEach(cost -> costsDto.add(this.costEntityToDto(cost)));
        
        return costsDto;
    }
    
    
    public ClientDto getClient(int id) {
       Clients client = clientsController.findClients(id);
       return this.clientEntityToDto(client);
    }
    
    public DeviceDto getDevice(int id) {
        Devices device = devicesController.findDevices(id);
        return this.deviceEntityToDto(device);
    }
    
    public HistoryDto getHistory(int id) {
        History history = historyController.findHistory(id);
        return this.historyEntityToDto(history);
    }
    
    public CostDto getCost(int id) {
        Costs cost = costsController.findCosts(id);
        return this.costEntityToDto(cost);
    }
    
    public void setClient(ClientDto clientDto) {
        Clients client = this.clientDtoToEntity(clientDto);
        clientsController.create(client);
    }
    
    public List<CostDto> getHistoryCosts(int historyId) {
        List<Costs> costs = this.historyController.findHistory(historyId).getCostsList();
        List<CostDto> costsDto = new ArrayList();
        
        costs.forEach(cost -> costsDto.add(this.costEntityToDto(cost)));
        
        return costsDto;
    }
    
    public String getHistoryToken(int historyId) {
        return historyController.findHistory(historyId).getTokens().getToken();
    }
    
    public void setDevice(DeviceDto deviceDto) {
        Devices device = this.deviceDtoToEntity(deviceDto);
        device.setParentId(selectedClient);
        this.devicesController.create(device);
        this.selectedClient = this.clientsController.findClients(this.selectedClient.getId());
       
    }
    
    public void setHistory(HistoryDto historyDto) {
        
            History history = this.historyDtoToEntity(historyDto);
            history.setParentId(selectedDevice);
                
            Calendar cal2 = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
            cal2.setTime(historyDto.getAcceptanceDate()); 
            
            int numOfOrders = this.historyController.findHistoryEntities().stream()
                    .filter(hist -> {
                       Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
                       cal1.setTime(hist.getAcceptanceDate()); 
                       
                       return cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) ;
                    })
                    .collect(Collectors.toList())
                    .size();
            
            numOfOrders += 1;
            String orderNumber = String.valueOf(numOfOrders) + "/" + (cal2.get(Calendar.MONTH)+1) + "/" + cal2.get(Calendar.YEAR);
            history.setOrderNumber(orderNumber);
            
            historyController.create(history);
            this.selectedDevice = this.devicesController.findDevices(this.selectedDevice.getId());
            
            
            History createdHistory = this.selectedDevice.getHistoryList().get(this.selectedDevice.getHistoryList().size() -1);
             Tokens token = new Tokens(); 
             token.setToken(Utils.randomCharacters(5));
             token.setHistory(createdHistory);
             token.setId(createdHistory.getId());
        try {
            tokensController.create(token);
        } catch (PreexistingEntityException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void setCost(CostDto costDto) {
        Costs cost = this.costDtoToEntity(costDto);
        cost.setParentId(selectedHistory);
        costsController.create(cost);
        this.selectedHistory = this.historyController.findHistory(this.selectedHistory.getId());
    }
    
    public void updateClient(ClientDto clientDto) {
        Clients client = this.clientsController.findClients(clientDto.getId());
        
        client.setName(clientDto.getName());
        client.setSurname(clientDto.getSurname());
        client.setAddress(clientDto.getAddress());
        client.setEmail(clientDto.getEmail());
        client.setPhone(clientDto.getPhone());
        client.setDescription(clientDto.getDescription());
        
        try {
            clientsController.edit(client);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateDevice(DeviceDto deviceDto) {
        Devices device = this.devicesController.findDevices(deviceDto.getId());
        
        device.setName(deviceDto.getName());
        device.setSerialNumber(deviceDto.getSerialNumber());
        device.setDescription(deviceDto.getDescription());
        
        try {
            devicesController.edit(device);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.selectedClient = this.clientsController.findClients(this.selectedClient.getId());
    }
    
    public void updateHistory(HistoryDto historyDto) {
        History history = this.historyController.findHistory(historyDto.getId());
        
        history.setName(historyDto.getName());
        history.setAcceptanceDate(historyDto.getAcceptanceDate());
        history.setDueDate(historyDto.getDueDate());
        history.setDescription(historyDto.getDescription());
        history.setStatus(historyDto.getStatus());
        try {
            historyController.edit(history);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         this.selectedDevice = this.devicesController.findDevices(this.selectedDevice.getId());
    }
    
    public void updateCostAcceptaction(int id, boolean value){
       Costs cost = this.costsController.findCosts(id);
       cost.setIsAccepted(value);
       
        try {
            costsController.edit(cost);
        } catch (Exception ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateCost(CostDto costDto) {
        Costs cost = this.costsController.findCosts(costDto.getId());
        
        cost.setName(costDto.getName());
        cost.setPrice(costDto.getPrice());
        cost.setVat(costDto.getVat());
        cost.setDiscount(costDto.getDiscount());
        cost.setDescription(costDto.getDescription());
        cost.setIsAccepted(costDto.isIsAccepted());
        try {
            costsController.edit(cost);
        } catch (Exception ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         this.selectedHistory = this.historyController.findHistory(this.selectedHistory.getId());
    }
    
    public void deleteClient(int clientId) {
        try {
            clientsController.destroy(clientId);
        } catch (IllegalOrphanException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteDevice(int deviceId) {
        try {
            devicesController.destroy(deviceId);
        } catch (IllegalOrphanException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteHistory(int historyId) {
        try {
            historyController.destroy(historyId);
        } catch (IllegalOrphanException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteCost(int CostId) {
        try {
            costsController.destroy(CostId);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(BackendFasade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setSelectedClient(ClientDto clientDto) {
        this.selectedClient = this.clientsController.findClients(clientDto.getId());
    }
    
    public ClientDto getSelectedClient() {
        return this.clientEntityToDto(this.selectedClient);
    }

    public DeviceDto getSelectedDevice() {
        return this.deviceEntityToDto(this.selectedDevice);
    }

    public void setSelectedDevice(DeviceDto selectedDevice) {
        this.selectedDevice = this.devicesController.findDevices(selectedDevice.getId());
    }

    public HistoryDto getSelectedHistory() {
        return this.historyEntityToDto(selectedHistory);
    }

    public void setSelectedHistory(HistoryDto selectedHistory) {
        this.selectedHistory = this.historyController.findHistory(selectedHistory.getId());
    }
    
    public HistoryDto clientLogin(String serviceNumber, String token) {
        List<History> histories = this.historyController.findHistoryEntities().stream()
                .filter(hist -> hist.getOrderNumber().equals(serviceNumber))
                .collect(Collectors.toList());
        if(histories.size() > 0){
            if(histories.get(0).getTokens().getToken().equals(token))
                return this.historyEntityToDto(histories.get(0));
        }
        return null;
    }
    
    
    private ClientDto clientEntityToDto(Clients client) {
        ClientDto tmpDto = new ClientDto();
        
        if(client != null) {
            tmpDto.setId(client.getId());
            tmpDto.setName(client.getName());
            tmpDto.setSurname(client.getSurname());
            tmpDto.setAddress(client.getAddress());
            tmpDto.setEmail(client.getEmail());
            tmpDto.setPhone(client.getPhone());
            tmpDto.setDescription(client.getDescription());
        }
        
        return tmpDto;
    }
    
    private DeviceDto deviceEntityToDto(Devices device) {
        DeviceDto tmpDto = new DeviceDto();
        
        if(device != null) {
            tmpDto.setId(device.getId());
            tmpDto.setClient(this.clientEntityToDto(device.getParentId()));
            tmpDto.setName(device.getName());
            tmpDto.setSerialNumber(device.getSerialNumber());
            tmpDto.setDescription(device.getDescription());
        }
        return tmpDto;
    }
    
    private HistoryDto historyEntityToDto(History history) {
        HistoryDto tmpDto = new HistoryDto();
        
        if(history != null) {
            tmpDto.setId(history.getId());
            tmpDto.setDevice(this.deviceEntityToDto(history.getParentId()));
            tmpDto.setName(history.getName());
            tmpDto.setAcceptanceDate(history.getAcceptanceDate());
            tmpDto.setDueDate(history.getDueDate());
            tmpDto.setDescription(history.getDescription());
            tmpDto.setOrderNumber(history.getOrderNumber());
            tmpDto.setStatus(history.getStatus() == null? 0: history.getStatus());
        }
        return tmpDto;
    }
    
    private CostDto costEntityToDto(Costs cost) {
        CostDto tmpDto = new CostDto();
        
        if(cost != null) {
            tmpDto.setId(cost.getId());
            tmpDto.setHistory(this.historyEntityToDto(cost.getParentId()));
            tmpDto.setName(cost.getName());
            tmpDto.setPrice(cost.getPrice());
            tmpDto.setVat(cost.getVat());
            tmpDto.setDiscount(cost.getDiscount());
            tmpDto.setDescription(cost.getDescription());
            tmpDto.setIsAccepted(cost.getIsAccepted() == null? false: cost.getIsAccepted());
        }
        return tmpDto;
    }
    
    private Clients clientDtoToEntity(ClientDto clientDto) {
        Clients tmpEntity = new Clients();
        tmpEntity.setId(clientDto.getId());
        tmpEntity.setName(clientDto.getName());
        tmpEntity.setSurname(clientDto.getSurname());
        tmpEntity.setAddress(clientDto.getAddress());
        tmpEntity.setEmail(clientDto.getEmail());
        tmpEntity.setPhone(clientDto.getPhone());
        tmpEntity.setDescription(clientDto.getDescription());
        
        return tmpEntity;
    }
    
    private Devices deviceDtoToEntity(DeviceDto deviceDto) {
        Devices tmpEntity = new Devices();
        tmpEntity.setName(deviceDto.getName());
        tmpEntity.setSerialNumber(deviceDto.getSerialNumber());
        tmpEntity.setDescription(deviceDto.getDescription());
        
        return tmpEntity;
    }
    
    private History historyDtoToEntity(HistoryDto historyDto) {
        History tmpEntity = new History();
        tmpEntity.setName(historyDto.getName());
        tmpEntity.setAcceptanceDate(historyDto.getAcceptanceDate());
        tmpEntity.setDueDate(historyDto.getDueDate());
        tmpEntity.setDescription(historyDto.getDescription());
        tmpEntity.setOrderNumber(historyDto.getOrderNumber());
        tmpEntity.setStatus(historyDto.getStatus());
        return tmpEntity;
    }
    
    private Costs costDtoToEntity(CostDto costDto) {
        Costs tmpEntity = new Costs();    
        tmpEntity.setName(costDto.getName());
        tmpEntity.setPrice(costDto.getPrice());
        tmpEntity.setVat(costDto.getVat());
        tmpEntity.setDiscount(costDto.getDiscount());
        tmpEntity.setDescription(costDto.getDescription());
        tmpEntity.setIsAccepted(costDto.isIsAccepted());
        return tmpEntity;
    }
}
