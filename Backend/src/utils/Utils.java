/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Wsiz
 */
public class Utils {
    public static String randomCharacters(int numChars) {
        int randomNum = 0;
        String result = "";
        for(int i = 0 ; i < numChars; ++i){
           randomNum =  ThreadLocalRandom.current().nextInt(49, 122 + 1);
           result += (char) randomNum;
        }
        
        return result;
    }
}
