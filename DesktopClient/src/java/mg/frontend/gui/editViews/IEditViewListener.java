package mg.frontend.gui.editViews;

import java.util.List;


public interface IEditViewListener {

    void okButtonListener(List<String> data);
}
