package mg.frontend.gui.editViews;

import dto.ClientDto;
import java.util.List;
import javax.swing.*;
import java.util.Map;
import mg.frontend.utils.DtoConverter;

public class ClientEditView extends EditView {
    private static final int WIDTH = 600;
    private static final int HEIGHT = 250;
    
    private String[] columns;
    private List<String> data;
    private  ClientDto client;
    
    public ClientEditView(String name,String[] columns, ClientDto client, IEditViewListener listener) {
        super(name, WIDTH, HEIGHT, listener);
        this.client = client;
        data = DtoConverter.clients2List(client);
        this.columns = columns;
        createWindow(columns.length, data);
    }

    @Override
    protected void addToPanel() {
        if(this.columns.length == this.data.size()-1){
          for(int i = 0; i < this.columns.length; ++i) {
               super.contentPanel.add(new JLabel(this.columns[i]));
               super.contentPanel.add(new JTextField(this.data.get(i+1)));
          }
        }
    }
}
