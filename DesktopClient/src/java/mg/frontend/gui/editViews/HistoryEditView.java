package mg.frontend.gui.editViews;

import dto.HistoryDto;
import java.util.List;
import javax.swing.*;
import java.util.Map;
import mg.frontend.utils.DtoConverter;

public class HistoryEditView extends EditView {
    private static final int WIDTH = 500;
    private static final int HEIGHT = 300;

    private String[] columns;
    private List<String> data;
    
    public HistoryEditView(String name,String[] columns, HistoryDto history, IEditViewListener listener) {
        super(name, WIDTH, HEIGHT, listener);
        this.data = DtoConverter.History2List(history);
        this.columns = columns;
        createWindow(this.columns.length,this.data);
    }

    @Override
    protected void addToPanel() {
         if(this.columns.length == this.data.size()-1){
          for(int i = 1; i < this.columns.length; ++i) {
               super.contentPanel.add(new JLabel(this.columns[i]));
               super.contentPanel.add(new JTextField(this.data.get(i+1)));
          }
        }
    }
}
