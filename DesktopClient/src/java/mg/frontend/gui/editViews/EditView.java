package mg.frontend.gui.editViews;

import java.awt.GridLayout;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;




public abstract class EditView extends JFrame {
    protected JPanel contentPanel;
    private IEditViewListener listener;

    public EditView(String name, int width, int height, IEditViewListener listener){
        this.listener = listener;
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setTitle(name);
        setSize(width,height);
        setResizable(false);

    }
    protected void createWindow(int rows ,List<String> data) {
        JPanel parentPanel = new JPanel();
        JPanel buttonsPanel = new JPanel(new GridLayout(1,2));
        this.contentPanel = new JPanel(new GridLayout(rows, 2));

        BoxLayout boxLayout = new BoxLayout(parentPanel,BoxLayout.Y_AXIS);
        parentPanel.setLayout(boxLayout);

        this.addToPanel();

        parentPanel.add(this.contentPanel);

        JButton okButton = new JButton("OK");
        okButton.addActionListener(e->
        {
            int k = 1;
            for(int i = 1; i <= rows; i++) {
                if(k < contentPanel.getComponents().length){
                    data.set(i, ((JTextField) contentPanel.getComponent(k)).getText());
                    k+=2;
                }
                else
                    break;
            }
            
            this.listener.okButtonListener(data);
            dispose();
        });

        buttonsPanel.add(okButton);

        JButton cancelButton = new JButton("Anuluj");
        cancelButton.addActionListener(e-> dispose());

        buttonsPanel.add(cancelButton);

        parentPanel.add(buttonsPanel);

        add(parentPanel);
        setVisible(true);
    }
    protected abstract void addToPanel();
}
