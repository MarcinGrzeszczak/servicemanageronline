package mg.frontend.gui.editViews;

import dto.DeviceDto;
import java.util.List;
import javax.swing.*;
import java.util.Map;
import mg.frontend.utils.DtoConverter;

public class DeviceEditView extends EditView {
    private static final int WIDTH = 500;
    private static final int HEIGHT = 150;

   private String[] columns;
    private List<String> data;
    private  DeviceDto client;
    public DeviceEditView(String name,String[] columns, DeviceDto device, IEditViewListener listener) {
        super(name, WIDTH, HEIGHT, listener);
        this.data = DtoConverter.devices2List(device);
        this.columns = columns;
        createWindow(columns.length,this.data);
    }

    @Override
    protected void addToPanel() {
         if(this.columns.length == this.data.size()-1){
          for(int i = 0; i < this.columns.length; ++i) {
               super.contentPanel.add(new JLabel(this.columns[i]));
               super.contentPanel.add(new JTextField(this.data.get(i+1)));
          }
        }
    }
}
