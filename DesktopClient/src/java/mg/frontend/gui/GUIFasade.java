package mg.frontend.gui;

import ejb.DesktopClientEjbRemote;
import javax.swing.*;

public class GUIFasade extends JFrame implements Runnable {

    private DesktopClientEjbRemote backend;
    
    private Thread guiThread;
    public GUIFasade(DesktopClientEjbRemote backend){
        this.backend = backend;
        this.createThread();
    }

    public void runInNewThread() {
        this.guiThread.start();
    }

    public void stopThread() {
        this.guiThread.interrupt();

        try {
            this.guiThread.join();
            this.createThread();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void createThread() {
        this.guiThread = new Thread(this);
    }

    @Override
    public void run() {
        new Window(backend);
    }
}
