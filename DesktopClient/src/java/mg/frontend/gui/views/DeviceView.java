package mg.frontend.gui.views;

//import mg.backend.BackendFasade;
import dto.DeviceDto;
import ejb.DesktopClientEjbRemote;
import mg.frontend.gui.customcomponents.*;
import mg.frontend.gui.editViews.DeviceEditView;
import mg.frontend.gui.editViews.IEditViewListener;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import mg.frontend.utils.DtoConverter;
import mg.frontend.utils.IConverterCallback;

public class DeviceView  implements IContentListener, ITableView, ButtonsListeners {

    private DesktopClientEjbRemote backend;
    private ContentComponent component;
    private TableContainer parentPanel;
    private ITableView childView;
    private ITableView parentView;
    private ITableContainerListener listener;
    private Map<String, String> tableMap;
    private IAnimationListener animListener;
    
    private List<DeviceDto> devices;
    private DeviceDto selectedDevice;
    private String[] columns;
    
    public DeviceView(DesktopClientEjbRemote backend, TableContainer parentPanel,ITableContainerListener listener,IAnimationListener animListener , ITableView childView ) {
        this.listener = listener;
        this.childView = childView;
        this.parentPanel = parentPanel;
        this.animListener = animListener;
        this.backend = backend;
     
        this.childView.setParentView(this);
        
        this.columns = new String[] {"name", "serial number", "description"};
        this.component = new ContentComponent(columns, this);
    }

    @Override
    public void setParentView(ITableView parentView) {
        this.parentView = parentView;
    }

    @Override
    public void selection(int id) {
          if(devices != null) {
            this.selectedDevice = DtoConverter.getDtoObjectFromId(devices, id);
            backend.setSelectedDevice(this.selectedDevice);
          }
        childView.load();
        this.listener.nextCardListener((ButtonsListeners) childView);
    }
        
    @Override
    public void load() {
        devices = backend.getClientDevices();
        List<List<String>> data = devices.stream()
                .map(item -> DtoConverter.devices2List(item))
                .collect(Collectors.toList());
        component.setData(data);
        
    }

    @Override
    public void rightClick(MouseEvent e) {
        RightContextMenu contextMenu = new RightContextMenu();
        contextMenu.addListeners(this::addListener, this::editListener, this::deleteListener);

        contextMenu.show(e.getComponent(),e.getX(), e.getY());
    }


    @Override
    public void addToContainer() {
        parentPanel.addToContainer(component.getComponent());
    }

    @Override
    public ContentComponent getContentComponent() {
        return this.component;
    }

    @Override
    public void backListener() {
        this.listener.previousCardListener((ButtonsListeners) this.parentView);
//        this.backend.setDeviceId(null);
//        this.backend.setClientId(null);
    }

    @Override
    public void addListener() {
        this.selectedDevice = new DeviceDto();
        DeviceEditView deviceEditView = new DeviceEditView("Dodaj Urzadzenie",this.columns,this.selectedDevice,this::okAddButtonListener);
    }

    @Override
    public void editListener() {
        int id = this.component.getselectedRowId();
        if(id != -1) {
            this.selectedDevice = DtoConverter.getDtoObjectFromId(devices, id);
            DeviceEditView deviceEditView = new DeviceEditView("Edytuj Urzadzenie", this.columns, this.selectedDevice, this::okEditButtonListener);
        }
    }

    @Override
    public void deleteListener() {
        int id = this.component.getselectedRowId();
        if(showConfirmationDelete() == JOptionPane.YES_OPTION && id != -1) {
            this.backend.deleteDevice(id);
            this.load();
            this.animListener.showDeleteAnimation();
        }
    }

    private int showConfirmationDelete() {
        return  JOptionPane.showConfirmDialog(null,
                "Czy na pewno chcesz usunac?", "Potwierdzenie Usuniecia",JOptionPane.YES_NO_OPTION);
    }


    public void okAddButtonListener(List<String> data) {
        DeviceDto dev = DtoConverter.list2DeviceDto(data);
        this.backend.setDevice(dev);
        this.load();
        this.animListener.showSaveAnimation();
    }

    public void okEditButtonListener(List<String> data) {
        DeviceDto dev = DtoConverter.list2DeviceDto(data);
        this.backend.updateDevice(dev);
        this.load();
        this.animListener.showSaveAnimation();
    }

}
