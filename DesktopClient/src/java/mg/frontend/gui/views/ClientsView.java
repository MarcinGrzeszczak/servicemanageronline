package mg.frontend.gui.views;

import dto.ClientDto;
import ejb.DesktopClientEjbRemote;
import javafx.geometry.Side;
import javafx.scene.control.ContextMenu;
import mg.frontend.gui.customcomponents.*;
import mg.frontend.gui.editViews.ClientEditView;
import mg.frontend.gui.editViews.IEditViewListener;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import mg.frontend.utils.DtoConverter;

public class ClientsView  implements IContentListener, ITableView, ButtonsListeners {

    private DesktopClientEjbRemote backend;
    private ContentComponent component;
    private TableContainer parentPanel;
    private ITableView childView;
    private ITableContainerListener listener;
    private Map<String, String> tableMap;
    private IAnimationListener animListener;
    
    private List<ClientDto> clients;
    private ClientDto selectedClient;
    private String[] columns;
    
    public ClientsView(DesktopClientEjbRemote backend, TableContainer parentPanel,ITableContainerListener listener, IAnimationListener animListener,ITableView childView ) {
        this.listener = listener;
        this.childView = childView;
        this.parentPanel = parentPanel;
        this.backend = backend;
        this.animListener = animListener;
        this.childView.setParentView(this);
        
        //TEST
        this.columns = new String[]{"name","surname","address","email","phone","description"};
        
        this.component = new ContentComponent(columns, this);

    }
    @Override
    public void setParentView(ITableView parentView) {
    }

    @Override
    public void selection(int id) {
        if(clients != null) {
            this.selectedClient = DtoConverter.getDtoObjectFromId(clients, id);
            backend.setSelectedClient(this.selectedClient);
            childView.load();
            this.listener.nextCardListener((ButtonsListeners) childView);
        }
    }

    @Override
    public void rightClick(MouseEvent e) {
        RightContextMenu contextMenu = new RightContextMenu();
        contextMenu.addListeners(this::addListener, this::editListener, this::deleteListener);

        contextMenu.show(e.getComponent(),e.getX(), e.getY());
    }

    @Override
    public void load() {
        
        clients = backend.getAllClients();
        List<List<String>> data = clients.stream()
                .map(item -> DtoConverter.clients2List(item))
                .collect(Collectors.toList());
        
        component.setData(data);
        
    }

    @Override
    public ContentComponent getContentComponent() {
        return this.component;
    }

    @Override
    public void addToContainer() {
        parentPanel.addToContainer(component.getComponent());
    }


    @Override
    public void backListener() {

    }

    @Override
    public void addListener() {
        this.selectedClient = new ClientDto();
        ClientEditView editView = new ClientEditView("Dodaj klienta",this.columns,this.selectedClient,this::okAddButtonListener);
    }

    @Override
    public void editListener() {
        int id = this.component.getselectedRowId();
        if(id != -1) {
            this.selectedClient = DtoConverter.getDtoObjectFromId(clients, id);
            ClientEditView editView = new ClientEditView("Dodaj klienta",this.columns, this.selectedClient,this::okEditButtonListener);
        }
    }

    @Override
    public void deleteListener() {
        int id = this.component.getselectedRowId();
        if(showConfirmationDelete() == JOptionPane.YES_OPTION && id != -1) {
            this.backend.deleteClient(id);
            this.load();
            this.animListener.showDeleteAnimation();
        }

    }

    private int showConfirmationDelete() {
        return  JOptionPane.showConfirmDialog(null,
                "Czy na pewno chcesz usunac?", "Potwierdzenie Usuniecia",JOptionPane.YES_NO_OPTION);
    }

    public void okAddButtonListener(List<String> data) {
        ClientDto cli = DtoConverter.list2ClientDto(data);
        this.backend.setClient(cli);
        this.load();
        this.animListener.showSaveAnimation();
    }

    public void okEditButtonListener(List<String> data) {
        ClientDto cli = DtoConverter.list2ClientDto(data);
        this.backend.updateClient(cli);
        this.load();
        this.animListener.showSaveAnimation();
    }




}
