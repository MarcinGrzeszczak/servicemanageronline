package mg.frontend.gui.views;

import dto.ClientDto;
import dto.DeviceDto;
import dto.HistoryDto;
import ejb.DesktopClientEjbRemote;
import java.awt.Desktop;
import mg.frontend.gui.customcomponents.*;
import mg.frontend.gui.editViews.HistoryEditView;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import mg.frontend.utils.DtoConverter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

public class HistoryView implements ITableView, IContentListener, ButtonsListeners {

    private DesktopClientEjbRemote backend;
    private ContentComponent component;
    private TableContainer parentPanel;
    private ITableView childView;
    private ITableView parentView;
    private ITableContainerListener listener;
    private Map<String, String> tableMap;
    private IAnimationListener animListener;
    
    private List<HistoryDto> historyList;
    private HistoryDto selectedHistory;
    private String[] columns;
    
    public HistoryView(DesktopClientEjbRemote backend, TableContainer parentPanel,ITableContainerListener listener,IAnimationListener animListener, ITableView childView ) {
        this.listener = listener;
        this.childView = childView;
        this.parentPanel = parentPanel;
        this.animListener = animListener;
        this.backend = backend;

        this.childView.setParentView(this);

        this.columns = new String[] {"orderNumber","name", "acceptanceDate","dueDate","description","status"};
        this.component = new ContentComponent(columns, this);
    }

    @Override
    public void setParentView(ITableView parentView) {
        this.parentView = parentView;
    }

    @Override
    public void selection(int id) {
        if(historyList != null) {
            this.selectedHistory = DtoConverter.getDtoObjectFromId(historyList, id);
            backend.setSelectedHistory(this.selectedHistory);
            childView.load();
            this.listener.nextCardListener((ButtonsListeners) childView);
        }
    }

    @Override
    public void rightClick(MouseEvent e) {
        int id = this.component.getselectedRowId();
        if(id != -1) {
            this.selectedHistory = DtoConverter.getDtoObjectFromId(historyList, id);
        }
        RightContextMenu contextMenu = new RightContextMenu();
        contextMenu.addListeners(this::addListener, this::editListener, this::deleteListener);
        contextMenu.enableFormGenerator(this::generateFormListener);
        contextMenu.show(e.getComponent(),e.getX(), e.getY());
    }

    
    public void generateFormListener() {
        ClientDto selectedClient = backend.getSelectedClient();
        DeviceDto selectedDevice = backend.getSelectedDevice();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try (PDDocument pdfDocument = PDDocument.load(new FileInputStream("resources/orderForm.pdf"))) {
            PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
            PDAcroForm form = docCatalog.getAcroForm();
        
            form.getField("description").setValue(this.selectedHistory.getDescription());
            form.getField("dueDate").setValue(dateFormat.format(this.selectedHistory.getDueDate()));
            form.getField("currentDate").setValue(dateFormat.format(new Date()));
            form.getField("orderName").setValue(this.selectedHistory.getName());
            form.getField("deviceName").setValue(selectedDevice.getName());
            form.getField("serialNumber").setValue(selectedDevice.getSerialNumber());
            form.getField("clientName").setValue(selectedClient.getName()+" "+selectedClient.getSurname());
            form.getField("clientAddress").setValue(selectedClient.getAddress());
            form.getField("token").setValue(backend.getHistoryToken(selectedHistory.getId()));
            form.getField("orderNumber").setValue(selectedHistory.getOrderNumber());
            for(PDField field : form.getFields())
                field.setReadOnly(true);
            
            pdfDocument.save("resources/tmp.pdf");
            pdfDocument.close();
            
            
            Desktop.getDesktop().open(new File("resources/tmp.pdf"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(HistoryView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HistoryView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void load() {
        historyList = backend.getDeviceHistory();
        List<List<String>> data = historyList.stream()
             .map(item -> DtoConverter.History2List(item))
             .collect(Collectors.toList());
        component.setData(data);
    }

    @Override
    public void addToContainer() {
        parentPanel.addToContainer(component.getComponent());
    }

    @Override
    public ContentComponent getContentComponent() {
        return this.component;
    }

    @Override
    public void backListener() {
        this.listener.previousCardListener((ButtonsListeners) this.parentView);
        //this.backend.setHistoryId(null);
        //this.backend.setDeviceId(null);
    }

    @Override
    public void addListener() {
        this.selectedHistory = new HistoryDto();
        HistoryEditView historyEditView = new HistoryEditView("Dodaj Historie", this.columns,this.selectedHistory,this::okAddButtonListener);
    }

    @Override
    public void editListener() {
        int id = this.component.getselectedRowId();
        if(id != -1) {
            this.selectedHistory = DtoConverter.getDtoObjectFromId(historyList, id);
            HistoryEditView historyEditView = new HistoryEditView("Edytuj Historie", this.columns,this.selectedHistory, this::okEditButtonListener);
        }
    }

    @Override
    public void deleteListener() {
        int id = this.component.getselectedRowId();
        if(showConfirmationDelete() == JOptionPane.YES_OPTION && id != -1) {
            this.backend.deleteHistory(id);
            this.load();
            this.animListener.showDeleteAnimation();
        }
    }

    private int showConfirmationDelete() {
        return  JOptionPane.showConfirmDialog(null,
                "Czy na pewno chcesz usunac?", "Potwierdzenie Usuniecia",JOptionPane.YES_NO_OPTION);
    }

    public void okAddButtonListener(List<String> data) {
        HistoryDto his = DtoConverter.list2HistoryDto(data);
        this.backend.setHistory(his);
        this.load();
        this.animListener.showSaveAnimation();
    }

    public void okEditButtonListener(List<String> data) {
        HistoryDto his = DtoConverter.list2HistoryDto(data);
        this.backend.updateHistory(his);
        this.load();
        this.animListener.showSaveAnimation();
    }
}
