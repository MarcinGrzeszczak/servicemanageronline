package mg.frontend.gui.views;

import dto.CostDto;
import ejb.DesktopClientEjbRemote;
import mg.frontend.gui.customcomponents.*;
import mg.frontend.gui.editViews.CostEditView;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import mg.frontend.utils.DtoConverter;

public class CostView implements ITableView, IContentListener, ButtonsListeners {

    private DesktopClientEjbRemote backend;
    private ContentComponent component;
    private TableContainer parentPanel;
    private ITableView parentView;
    private ITableContainerListener listener;
    private IAnimationListener animListener;
    
    private List<CostDto> costs;
    private CostDto selectedCost;
    private String[] columns;
    
    public CostView(DesktopClientEjbRemote backend, TableContainer parentPanel,ITableContainerListener listener,IAnimationListener animListener ,ITableView childView ) {
        this.listener = listener;
        this.parentPanel = parentPanel;
        this.backend = backend;
        this.animListener = animListener;
        
        this.columns = new String[]{"name","price","vat","discount","description","isAccepted"};
        
        this.component = new ContentComponent(columns, this);
    }

    @Override
    public void setParentView(ITableView parentView) {
        this.parentView = parentView;
    }

    @Override
    public void selection(int id) {
       this.selectedCost = DtoConverter.getDtoObjectFromId(costs, id);
    }

    @Override
    public void load() {
        costs = backend.getHistoryCosts();
        List<List<String>> data = costs.stream()
                .map(item -> DtoConverter.costs2List(item))
                .collect(Collectors.toList());
        
        component.setData(data);
    }

    @Override
    public void addToContainer() {
        parentPanel.addToContainer(component.getComponent());
    }

    @Override
    public ContentComponent getContentComponent() {
        return this.component;
    }

    @Override
    public void rightClick(MouseEvent e) {
        RightContextMenu contextMenu = new RightContextMenu();
        contextMenu.addListeners(this::addListener, this::editListener, this::deleteListener);

        contextMenu.show(e.getComponent(),e.getX(), e.getY());
    }


    @Override
    public void backListener() {

        this.listener.previousCardListener((ButtonsListeners) this.parentView);
//        this.backend.setCostsId(null);
//        this.backend.setHistoryId(null);
    }

    @Override
    public void addListener() {
        this.selectedCost = new CostDto();
        CostEditView costEditView = new CostEditView("Dodaj koszty",this.columns,this.selectedCost, this::okAddButtonListener);
    }

    @Override
    public void editListener() {
        int id = this.component.getselectedRowId();
        if(id != -1) {
            this.selectedCost = DtoConverter.getDtoObjectFromId(costs, id);
          
            CostEditView costEditView = new CostEditView("Edytuj koszty",this.columns,this.selectedCost, this::okEditButtonListener);
        }
    }

    @Override
    public void deleteListener() {
        int id = this.component.getselectedRowId();
        if(showConfirmationDelete() == JOptionPane.YES_OPTION && id != -1) {
            this.backend.deleteCost(id);
            this.load();
            this.animListener.showDeleteAnimation();
        }

    }

    private int showConfirmationDelete() {
        return  JOptionPane.showConfirmDialog(null,
                "Czy na pewno chcesz usunac?", "Potwierdzenie Usuniecia",JOptionPane.YES_NO_OPTION);
    }

    public void okAddButtonListener(List<String> data) {
        CostDto co = DtoConverter.list2CostDto(data);
        this.backend.setCost(co);
        this.load();
        this.animListener.showSaveAnimation();
    }

    public void okEditButtonListener(List<String> data) {
        CostDto co = DtoConverter.list2CostDto(data);
        this.backend.updateCost(co);
        this.load();
        this.animListener.showSaveAnimation();
    }
}
