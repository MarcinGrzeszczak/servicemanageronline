/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.frontend.utils;

import dto.ClientDto;
import dto.CostDto;
import dto.DeviceDto;
import dto.Dto;
import dto.HistoryDto;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Wsiz
 */
public class DtoConverter {
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static List<String> clients2List(ClientDto client) {
        List<String> list = new ArrayList();
        list.add(String.valueOf(client.getId()));
        list.add(client.getName());
        list.add(client.getSurname());
        list.add(client.getAddress());
        list.add(client.getEmail());
        list.add(client.getPhone());
        list.add(client.getDescription());
        
        return list;
    }
    
     public static List<String> devices2List(DeviceDto device) {
        List<String> list = new ArrayList();
        list.add(String.valueOf(device.getId()));
        list.add(device.getName());
        list.add(device.getSerialNumber());
        list.add(device.getDescription());
        
        return list;
    }
     
    public static List<String> History2List(HistoryDto history) {
        List<String> list = new ArrayList();
        list.add(String.valueOf(history.getId()));
        list.add(history.getOrderNumber());
        list.add(history.getName());
        list.add( dateFormat.format(history.getAcceptanceDate()));
        list.add(dateFormat.format(history.getDueDate()));
        list.add(history.getDescription());
        list.add(String.valueOf(history.getStatus()));
        
        return list;
    }
    
     public static List<String> costs2List(CostDto cost) {
        List<String> list = new ArrayList();
        list.add(String.valueOf(cost.getId()));
        list.add(cost.getName());
        list.add(String.valueOf(cost.getPrice()));
        list.add(String.valueOf(cost.getVat()));
        list.add(String.valueOf(cost.getDiscount()));
        list.add(cost.getDescription());
        list.add(String.valueOf(cost.isIsAccepted()));
        
        return list;
    }
    
    public static ClientDto list2ClientDto(List<String> list) {
        ClientDto client = new ClientDto();
        client.setId(Integer.parseInt(list.get(0)) );
        client.setName(list.get(1));
        client.setSurname(list.get(2));
        client.setAddress(list.get(3));
        client.setEmail(list.get(4));
        client.setPhone(list.get(5));
        client.setDescription(list.get(6));
        
        return client;
    } 
    
    public static CostDto list2CostDto(List<String> list) {
        CostDto cost = new CostDto();
        cost.setId(Integer.parseInt(list.get(0)) );
        cost.setName(list.get(1));
        cost.setPrice(Float.parseFloat(list.get(2)));
        cost.setVat(Float.parseFloat(list.get(3)));
        cost.setDiscount(Float.parseFloat(list.get(4)));
        cost.setDescription(list.get(5));
        cost.setIsAccepted(list.get(6).toLowerCase().equals("true"));
        return cost;
    } 
    
     public static DeviceDto list2DeviceDto(List<String> list) {
        DeviceDto device = new DeviceDto();
        device.setId(Integer.parseInt(list.get(0)) );
        device.setName(list.get(1));
        device.setSerialNumber(list.get(2));
        device.setDescription(list.get(3));
        
        return device;
    } 
     
    public static HistoryDto list2HistoryDto(List<String> list) {
        HistoryDto history = new HistoryDto();
        history.setId(Integer.parseInt(list.get(0)) );
        //history.setOrderNumber(list.get(1));
        history.setName(list.get(1));
        history.setAcceptanceDate(Date.valueOf(list.get(2)));
        history.setDueDate(Date.valueOf(list.get(3)));
        history.setDescription(list.get(4));
        history.setStatus(Integer.parseInt(list.get(5)));

        return history;
    } 
     
    public static<T extends Dto> T getDtoObjectFromId(List<T> dtoList ,int id) {
        return dtoList.stream()
                .filter(item->item.getId() == id)
                .collect(Collectors.toList())
                .get(0);
    }
}

    
