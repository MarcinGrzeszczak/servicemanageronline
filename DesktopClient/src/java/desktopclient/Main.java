/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desktopclient;

import ejb.DesktopClientEjbRemote;
import javax.ejb.EJB;
import mg.frontend.gui.GUIFasade;

/**
 *
 * @author Wsiz
 */
public class Main {

    @EJB
    private static DesktopClientEjbRemote desktopClientEjb;

    /**
     * @param args the command line arguments
     */
    
       public Main() {
        //new Console();
        GUIFasade gui = new  GUIFasade(desktopClientEjb);
        gui.runInNewThread();
    }
    
    public static void main(String[] args) {
       Main app = new Main();
    }
    
}
