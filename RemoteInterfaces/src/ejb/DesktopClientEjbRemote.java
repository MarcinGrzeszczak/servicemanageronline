/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import dto.ClientDto;
import dto.CostDto;
import dto.DeviceDto;
import dto.HistoryDto;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Wsiz
 */
@Remote
public interface DesktopClientEjbRemote {
    
        public List<ClientDto> getAllClients();
        public List<DeviceDto> getClientDevices();
        public List<HistoryDto> getDeviceHistory();
        public List<CostDto> getHistoryCosts();
        
        public ClientDto getClient(int id);
        public DeviceDto getDevice(int id);
        public HistoryDto getHistory(int id);
        public CostDto getCost(int id);
        public String getHistoryToken(int historyId);
        
        
        public void setClient(ClientDto clientDto);
        public void setDevice(DeviceDto deviceDto);
        public void setHistory(HistoryDto historyDto);
        public void setCost(CostDto costDto);
        public void setSelectedClient(ClientDto client);
        public void setSelectedDevice(DeviceDto device);
        public void setSelectedHistory(HistoryDto history);
        
        public ClientDto getSelectedClient();
        public DeviceDto getSelectedDevice();
        public HistoryDto getSelectedHistory();
        
        public void updateClient(ClientDto clientDto);
        public void updateDevice(DeviceDto deviceDto);
        public void updateHistory(HistoryDto historyDto);
        public void updateCost(CostDto costDto);
        
        public void deleteClient(int clientId);
        public void deleteDevice(int deviceId);
        public void deleteHistory(int historyId);
        public void deleteCost(int CostId);
}
