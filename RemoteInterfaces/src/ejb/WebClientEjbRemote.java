/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import dto.CostDto;
import dto.HistoryDto;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Wsiz
 */
@Remote
public interface WebClientEjbRemote {
    public HistoryDto loginClient(String serviceNumber, String token);
    public HistoryDto getHistory(int id);
    public List<CostDto> getHistoryCosts(int historyId);
    public void updateCostAcceptaction(int id, boolean value);
    
}
