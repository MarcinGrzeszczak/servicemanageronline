## Stack
* Glassfish 4.1
* Java EE 7
* Java JDK 8


## IDE
* NetBeans 11.4


## Uruchomienie
Należy ściągnąć **Postgres jdbc** [jar] i skopiować do `%GLASSFISH_DIR%/glassfish/domains/domain1/lib`

[link do Postgres jdbc](https://jdbc.postgresql.org/download/postgresql-42.2.13.jre7.jar)

**Następnie Clean and Build  w kolejności**
* AppCommunication
* RemoteInterfaces
* Backend
* ServiceManagerServerApp
* DesktopCLient
* WebClient

**Deploy:**
* ServiceManagerServerApp
* DesktopClient
* WebClient