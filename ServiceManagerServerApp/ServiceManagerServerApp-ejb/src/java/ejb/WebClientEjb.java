/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import dto.CostDto;
import dto.HistoryDto;
import java.util.List;
import javax.ejb.Stateless;
import logic.BackendFasade;

/**
 *
 * @author Wsiz
 */
@Stateless
public class WebClientEjb implements WebClientEjbRemote {
    
    private static final BackendFasade fasade = new BackendFasade();
    
    @Override
    public HistoryDto loginClient(String serviceNumber, String token) {
       return fasade.clientLogin(serviceNumber, token);
    }

    @Override
    public HistoryDto getHistory(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
     @Override
    public List<CostDto> getHistoryCosts(int historyId) {
        return fasade.getHistoryCosts(historyId);
    }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public void updateCostAcceptaction(int id, boolean value) {
        fasade.updateCostAcceptaction(id, value);
    }
}
