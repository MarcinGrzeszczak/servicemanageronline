/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import dto.ClientDto;
import dto.CostDto;
import dto.DeviceDto;
import dto.HistoryDto;
import java.util.List;
import javax.ejb.Stateless;
import logic.BackendFasade;

/**
 *
 * @author Wsiz
 */

@Stateless
public class DesktopClientEjb implements DesktopClientEjbRemote {
    
    private static final BackendFasade fasade = new BackendFasade();
    
    @Override
    public List<ClientDto> getAllClients() {
        return fasade.getAllClients();
    }

    @Override
    public List<DeviceDto> getClientDevices() {
        return fasade.getClientDevices();
    }

    @Override
    public List<HistoryDto> getDeviceHistory() {
        return fasade.getDeviceHistory();
    }

    @Override
    public List<CostDto> getHistoryCosts() {
        return fasade.getHistoryCosts();
    }

    @Override
    public ClientDto getClient(int id) {
        return fasade.getClient(id);
    }

    @Override
    public DeviceDto getDevice(int id) {
        return fasade.getDevice(id);
    }

    @Override
    public HistoryDto getHistory(int id) {
        return fasade.getHistory(id);
    }

    @Override
    public CostDto getCost(int id) {
        return fasade.getCost(id);
    }

    @Override
    public void setClient(ClientDto clientDto) {
        fasade.setClient(clientDto);
    }

    @Override
    public void setDevice(DeviceDto deviceDto) {
        fasade.setDevice(deviceDto);
    }

    @Override
    public void setHistory(HistoryDto historyDto) {
        fasade.setHistory(historyDto);
    }

    @Override
    public void setCost(CostDto costDto) {
        fasade.setCost(costDto);
    }

    @Override
    public void updateClient(ClientDto clientDto) {
        fasade.updateClient(clientDto);
    }

    @Override
    public void updateDevice(DeviceDto deviceDto) {
        fasade.updateDevice(deviceDto);
    }

    @Override
    public void updateHistory(HistoryDto historyDto) {
        fasade.updateHistory(historyDto);
    }

    @Override
    public void updateCost(CostDto costDto) {
        fasade.updateCost(costDto);
    }

    @Override
    public void deleteClient(int clientId) {
        fasade.deleteClient(clientId);
    }

    @Override
    public void deleteDevice(int deviceId) {
        fasade.deleteDevice(deviceId);
    }

    @Override
    public void deleteHistory(int historyId) {
        fasade.deleteHistory(historyId);
    }

    @Override
    public void deleteCost(int CostId) {
        fasade.deleteCost(CostId);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public void setSelectedClient(ClientDto client) {
       fasade.setSelectedClient(client);
    }

    @Override
    public void setSelectedDevice(DeviceDto device) {
        fasade.setSelectedDevice(device);
    }

    @Override
    public void setSelectedHistory(HistoryDto history) {
        fasade.setSelectedHistory(history);
    }

    @Override
    public ClientDto getSelectedClient() {
        return fasade.getSelectedClient();
    }

    @Override
    public DeviceDto getSelectedDevice() {
        return fasade.getSelectedDevice();
    }

    @Override
    public HistoryDto getSelectedHistory() {
        return fasade.getSelectedHistory();
    }
     @Override
     public String getHistoryToken(int historyId) {
         return fasade.getHistoryToken(historyId);
     }
}
